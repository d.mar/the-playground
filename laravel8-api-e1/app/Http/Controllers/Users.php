<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use App\Models\Post;

class Users extends Controller
{
    public function showAllByRating()
    {
        $users = User::select()->orderBy('avg_ratings')->get()->makeHidden(['created_at', 'updated_at']);
        foreach ($users as $user) {
            $user->posts = Post::where('author', $user->id)->get();
        }

        return $users->values();
    }
}
