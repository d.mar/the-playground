<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Post;
use App\Models\User;

class Posts extends Controller
{
    public function show(Request $request)
    {
        // Get that post.
        $post = Post::find($request->id);

        // If not exists, 404.
        if (false == $post) {
            abort(404);
        }
        return response()->json([
            'id' => $post->id,
            'author' => User::where('id', $post->author)->get('name')[0]['name'],
            'title' => $post->title,
            'body' => $post->body,
        ], 200);
    }
    public function top()
    {
        $posts = Post::select(['id', 'author', 'title', 'body', 'rating'])
            ->orderByDesc('rating')
            ->get()
            ->unique('author');

        return $posts->values();
    }
}
