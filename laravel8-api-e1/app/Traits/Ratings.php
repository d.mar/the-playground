<?php

namespace App\Traits;

trait Ratings
{
    public function calcPostRating(string $title, string $body) : int
    {
        $rating_title   = (count(explode(' ', $title)) * 2);
        $rating_body    = count(explode(' ', str_replace("\n", ' ', $body)));

        return ($rating_title + $rating_body);
    }
}
