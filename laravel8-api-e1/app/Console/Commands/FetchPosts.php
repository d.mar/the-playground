<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Post;
use App\Traits\Ratings;

class FetchPosts extends Command
{
    use Ratings;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch posts from jsonplaceholder.typicode.com';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $postLimit = 50;

        // Request to posts endpoint.
        $request = Http::get('https://jsonplaceholder.typicode.com/posts');

        // Retrieve request data as collection.
        $posts = $request->collect()
            ->slice(0, $postLimit);

        // Calc post rating and store or update db.
        foreach ($posts as $post) {
            Post::updateOrInsert(
                ['id' => $post['id'], 'title' => $post['title'], 'author' => $post['userId']],
                ['body' => $post['body'], 'rating' => $this->calcPostRating($post['title'], $post['body'])],
            );
        }
        return 0;
    }
}
