<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\Post;
use App\Models\User;

class FetchUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fetch:users_with_posts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::select('id')->get()->toArray();
        $posts = Post::select('author')->distinct()->whereNotIn('author', $users)->get();

        foreach ($posts as $post) {
            $request = Http::get('https://jsonplaceholder.typicode.com/users/' . $post->author);
            $user = $request->collect();
            User::create([
                'id' => $user['id'],
                'name' => $user['name'],
                'email' => $user['email'],
                'city' => $user['address']['city'],
            ]);
        }

        return 0;
    }
}
