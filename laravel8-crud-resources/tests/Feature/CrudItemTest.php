<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;
use App\Models\Item;

class CrudItemTest extends TestCase
{
    /**
     * Anon user can list items.
     *
     * @return void
     */
    public function test_anon_user_can_index_items()
    {
        $item = Item::factory()->create();

        $response = $this->get('/items');

        $response->assertStatus(200);
        $response->assertSee($item->name);
    }
    /**
     * Anon user can show item.
     *
     * @return void
     */
    public function test_anon_user_can_show_item()
    {
        $item = Item::factory()->create();

        $response = $this->get('/items/' . $item->id);

        $response->assertStatus(200);
        $response->assertSee($item->name);
    }

    /**
     * Anon user can create item.
     *
     * @return void
     */
    public function test_anon_user_can_store_item()
    {
        Session::start();
        $item = Item::factory()->definition();
        $item += ['_token' => csrf_token()];
        $itemCount = Item::all()->count();

        $this->post('/items', $item)->assertStatus(302);
        $this->assertEquals(($itemCount + 1), Item::all()->count());
    }

    /**
     * Anon user can destroy item.
     *
     * @return void
     */
    public function test_anon_user_can_destroy_item()
    {
        Session::start();
        $itemId = Item::latest()->first()->id;
        $itemCount = Item::all()->count();

        $this->delete('/items/' . $itemId, ['_token' => csrf_token()])->assertStatus(302);
        $this->assertEquals(($itemCount - 1), Item::all()->count());
    }

    /**
     * Anon user can update item.
     *
     * @return void
     */
    public function test_anon_user_can_update_item()
    {
        Session::start();
        $itemId = Item::latest()->first()->id;
        $itemEdit = Item::factory()->definition();
        $itemEdit += ['_token' => csrf_token()];
        $itemCount = Item::all()->count();

        $this->put('/items/' . $itemId, $itemEdit)->assertStatus(302);
        $this->assertEquals(($itemCount), Item::all()->count());
    }

    /**
     * A item require a name.
     *
     * @return void
     */
    public function test_store_item_require_name()
    {
        Session::start();
        $item = Item::factory()->make(['name' => null]);
        $item = $item->toArray();
        $item += ['_token' => csrf_token()];

        $this->post('/items', $item)
             ->assertSessionHasErrors();
    }

    /**
     * A item require a name.
     *
     * @return void
     */
    public function test_store_item_require_description()
    {
        Session::start();
        $item = Item::factory()->make(['description' => null]);
        $item = $item->toArray();
        $item += ['_token' => csrf_token()];

        $this->post('/items', $item)
             ->assertSessionHasErrors();
    }
}
