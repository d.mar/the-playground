<?php

namespace App\Http\Controllers;

use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

class ItemResourceController extends Controller
{
    /**
     * Path to public storage
     */
    private $imageStorage = 'uploads/items/images';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::orderByDesc('id')->paginate(10);

        return view('items.index', compact('items'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048|nullable',
        ]);

        if ($request->image) {

            $postData = [
                'name' => $request->name,
                'description' => $request->description,
                'image' => $this->storeImage($request->image)
            ];

        } else {

            $postData = [
                'name' => $request->name,
                'description' => $request->description,
                'image' => null
            ];
        }

        if (Item::create($postData)) {
            return redirect()->route('items.index')
                ->with('success','Item created successfully.');
        }

        return redirect()->route('items.index')
                ->with('failure','Item can not be created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return view('items.show',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view('items.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:4048|nullable',
        ]);

        $postData = $request->all();

        if ($request->image) {

            $postData = [
                'name' => $request->name,
                'description' => $request->description,
                'image' => $this->storeImage($request->image)
            ];
        } elseif ($request->delete) {
            $this->destroyImage($item->image);
        }

        if ($item->update($postData)) {
            return redirect()->route('items.index')
                ->with('success','Item updated successfully');
        }

        return redirect()->route('items.index')
            ->with('failure','Item can not be updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        if ($item->image) {
            $this->destroyImage($item->image);
        }
        if ($item->delete()) {

            return redirect()->route('items.index')
                ->with('success','Item deleted successfully.');
        }
        return redirect()->route('items.index')
                ->with('failure','Item can not be deleted.');
    }

    /**
     * Store item image.
     *
     * @param Illuminate\Http\UploadedFile $image
     * @return String
     */
    private function storeImage(UploadedFile $image) : String
    {
        $path = 'public/' . $this->imageStorage;

        if (File::exists($path)) {
            File::delete($path);
        }
        $image->store($path);
        return $image->hashName();
    }

    /**
     * Remove item image.
     *
     * @param String $file
     * @return Bool
     */
    private function destroyImage(String $file) : Bool
    {
        $path = 'storage/' . $this->imageStorage . '/' . $file;
        // dd(File::exists($path), $path);
        if (File::exists($path)) {
            return File::delete($path);
        }

        return false;
    }
}
