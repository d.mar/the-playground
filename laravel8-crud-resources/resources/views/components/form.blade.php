<div class="container">

    <div class="row mb-3">

        <div class="col">
            <label class="form-label" for="name">Name: </label>
            <input
                type="text"
                name="name"
                class="form-control @error('name') is-invalid @enderror"
                placeholder="Item name"
                value="{{ $item->name ?? ''  }}">
        </div>

    </div><!-- .row -->
    <div class="row mb-3">

        <div class="col">
            <label class="form-label" for="description">Description:</label>
            <textarea class="form-control @error('description') is-invalid @enderror"name="description" placeholder="Item description">{{ $item->description ?? '' }}</textarea>
        </div>

    </div><!-- .row -->
    <div class="row mb-3">

        @isset($item->image)
        <div class="col-2">
            <img src="{{ asset('storage/uploads/items/images/' . $item->image) }}" class="img-thumbnail" alt="" />
        </div>
        @endif
        <div class="col">

            <label class="form-label" for="image">Image:</label>
            <input type="file" placeholder="Item image" name="image" id="file" accept="image/*" class="form-control @error('image') is-invalid @enderror" />

            @isset($item->image)
            <label class="form-label" for="image">Remove image</label>
            <input type="checkbox" name="delete"  />
            @endif

        </div>

    </div><!-- .row -->
    <div class="row mt-4">

        <div class="col text-end">
            <button type="reset" class="btn btn-warning m-1 rounded-0 border shadow-sm">Reset</button>
            <button type="submit" class="btn btn-primary m-1 rounded-0 border shadow-sm">Submit</button>
        </div>

    </div><!-- .row -->

</div>
