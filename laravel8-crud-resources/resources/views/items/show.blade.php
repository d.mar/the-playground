@extends('layouts.crud')

@section('content')

<div class="row">
    <div class="col my-3 text-center">
        <h2 class="h6 text-primary"> - {{ $item->name }} -</h2>
    </div>
</div><!-- .row -->

 <div class="row my-3">
    <div class="col">
        <h2>{{ $item->name }}</h2>
    </div>
</div><!-- .row -->
<div class="row">

     @if ($item->image)
     <div class="col-2">
        <img src="{{ asset('storage/uploads/items/images/' . $item->image) }}" alt="{{ $item->description }}" />
    </div>
    @endif

    <div class="col">
        {{ $item->description }}
    </div>

</div><!-- .row -->

@endsection
