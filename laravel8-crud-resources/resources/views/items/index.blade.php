@extends('layouts.crud')

@section('content')

<div id="list">

    @include('components.pagination')

    @isset($items)
    <div class="table-responsive">

        <table class="table table-striped table-hover">

            <thead>
                <tr>
                    <th class="th-sm">ID</th>
                    <th class="th-lg">Image</th>
                    <th class="th-lg">Name</th>
                    <th class="th-lg">Action</th>
                </tr>
            </thead>
            @foreach($items as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>
                    @if($item->image)
                        <img src="{{ asset('storage/uploads/items/images/' . $item->image) }}" alt="{{ $item->description }}" class="border rounded my-1 mx-3" height="35" />
                    @else
                        -
                    @endif
                </td>
                <td>{{ $item->name }}</td>
                <td class="text-end">
                   <form action="{{ route('items.destroy',$item->id) }}" method="POST">

                        <a class="btn btn-info btn-sm m-2" href="{{ route('items.show',$item->id) }}">Show</a>
                        <a class="btn btn-primary btn-sm m-2" href="{{ route('items.edit',$item->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger btn-sm m-2" >Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table>

    </div><!-- .table-responsive -->
    @endif

   @include('components.pagination')

</div><!-- #list -->

@endsection
