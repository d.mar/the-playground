@extends('layouts.crud')

@section('content')

<div class="row">
    <div class="col my-3 text-center">
        <h2 class="h6 text-primary"> - Add new Item -</h2>
    </div>
</div><!-- .row -->

@if ($errors->any())
<div class="row">
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div><!-- .row -->
@endif

<div class="row">
    <div class="col">
        <form action="{{ route('items.update', $item->id) }}" method="POST" enctype="multipart/form-data" class="bg-light border py-4 px-3 shadow">
            @csrf
            @method('PUT')
            @include('components.form')
        </form>
    </div>
</div><!-- .row -->

@endsection
