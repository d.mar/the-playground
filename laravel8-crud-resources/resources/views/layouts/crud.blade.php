<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel 8 CRUD with [Items]') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="font-sans antialiased">

        <div class="min-h-screen bg-gray-100">

            <main class="container">

                <!-- Nav -->
                @include('components.nav')

                <!-- Messages -->
                <div id="messages" class="row my-3">

                    <div class="col">

                        @if($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @elseif($message = Session::get('failure'))
                            <div class="alert alert-warning">
                                <p>{{ $message }}</p>
                            </div>
                        @endif

                    </div>

                </div><!-- .row -->

                <!-- Content -->
                <div class="row">

                    <div class="col">@yield('content')</div>

                </div><!-- .row -->

            </main><!-- .container -->

        </div>
    </body>
</html>
