<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\ItemResourceController;

Route::get('/', function () {
    return redirect('items');
});
Route::resource('items', ItemResourceController::class);
